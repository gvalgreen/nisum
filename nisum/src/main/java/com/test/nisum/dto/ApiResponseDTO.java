package com.test.nisum.dto;

import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public class ApiResponseDTO<T> {
  @JsonView(Views.UserViews.class)
  private T data;
  @JsonView(Views.UserViews.class)
  private Notification notification;

  private ApiResponseDTO() {

  }

  public ApiResponseDTO(Notification notification) {
    this.notification = notification;
  }

  public ApiResponseDTO(T data, Notification notification) {
    this.notification = notification;
    this.data = data;
  }


  public T getData() {
    return this.data;
  }

  public Notification getNotification() {
    return this.notification;
  }

}
