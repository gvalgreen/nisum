package com.test.nisum.dto;

import com.test.nisum.domain.model.User;
import lombok.Data;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Data
public class PhoneDto {

  private Long id;
  private String number;
  private String cityCode;
  private String countryCode;
  private UserDto user;
}
