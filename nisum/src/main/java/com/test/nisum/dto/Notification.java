package com.test.nisum.dto;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;

/**
 * @author gvalgreen
 * @version 1.0.0
 */

@Getter
public class Notification {

  @JsonView(Views.UserViews.class)
  private String message;

  private Notification() {

  }

  private Notification(Notification.Builder builder) {
    this.message = builder.message;
  }


  public static Notification.Builder builder(String description) {
    return new Notification.Builder(description);
  }




  public static class Builder {

    private String message;

    public Builder(String message) {
      this.message = message;
    }

    public Notification build() {
      return new Notification(this);
    }
  }
}
