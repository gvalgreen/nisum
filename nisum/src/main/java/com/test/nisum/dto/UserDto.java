package com.test.nisum.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.test.nisum.domain.model.Phone;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Data;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Data
public class UserDto {

  @JsonView(Views.UserViews.class)
  private UUID id;
  @NotEmpty(message = "Please provide a name")
  private String name;
  @NotEmpty(message = "Please provide an email")
  @Email(regexp = ".+@.+\\..+", message = "Email format is invalid")
  private String email;
  @NotEmpty(message = "Please provide an password")
  @Pattern(regexp = "[A-Z]{1}.*[a-z][0-9]{1,2}[:.,-]?$", message = "Password format is invalid. the correct format is: 1 UPPERCASE, any lowercase and 2 numbers")
  private String password;
  @JsonView(Views.UserViews.class)
  private boolean isActive;
  @JsonView(Views.UserViews.class)
  private UUID token;
  private List<PhoneDto> phones;
  @JsonView(Views.UserViews.class)
  private LocalDateTime created;
  @JsonView(Views.UserViews.class)
  private LocalDateTime modified;
  @JsonView(Views.UserViews.class)
  private LocalDateTime lastLogin;
}
