package com.test.nisum.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Getter
@AllArgsConstructor
public enum ApiResponseCode {
  SUCCESS("NISUM-OK", "Request executed successfully", HttpStatus.OK),
  INTERNAL_ERROR("NISUM-ERR-00", "Internal application error", HttpStatus.INTERNAL_SERVER_ERROR),
  NOT_FOUND("NISUM-ERR-01", "There aren't data for Id sent", HttpStatus.NOT_FOUND),
  NOT_CONTENT("NISUM-ERR-02", "There aren't data for resources request", HttpStatus.NO_CONTENT),
  BAD_REQUEST("NISUM-ERR-03", "Bad Request", HttpStatus.BAD_REQUEST),
  EMAIL_REGISTERED("NISUM-ERR-04", "The email is already registered", HttpStatus.BAD_REQUEST),
  EMAIL_NOT_FOUND("NISUM-ERR-05", "The email sent isn't registered", HttpStatus.BAD_REQUEST),
  USER_DELETE_ERROR("NISUM-ERR-06", "An error occurred deleting the user",
      HttpStatus.INTERNAL_SERVER_ERROR);
  private String code;
  private String description;
  private HttpStatus httpStatus;
}
