package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class NisumNotContentException extends ApiError {


  public NisumNotContentException() {
    super(ApiResponseCode.NOT_CONTENT);
  }


}
