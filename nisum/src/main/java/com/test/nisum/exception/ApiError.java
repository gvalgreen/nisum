package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.http.HttpStatus;

@ToString
@Getter
public class ApiError extends Error {

  private final HttpStatus httpStatus;


  public ApiError(ApiResponseCode apiResponseCode) {
    super(apiResponseCode);
    this.httpStatus = apiResponseCode.getHttpStatus();
  }
}
