package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@AllArgsConstructor
@Getter
@ToString
public class Error {

  @NonNull
  private String code;

  @NonNull
  private String message;

  @NonNull
  private HttpStatus httpStatus;


  public Error(ApiResponseCode apiResponseCode) {
    this.code = apiResponseCode.getCode();
    this.message = apiResponseCode.getDescription();
    this.httpStatus = apiResponseCode.getHttpStatus();
  }

}
