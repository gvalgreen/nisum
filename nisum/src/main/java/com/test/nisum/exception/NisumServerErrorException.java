package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class NisumServerErrorException extends ApiError {

    public NisumServerErrorException() {
        super(ApiResponseCode.INTERNAL_ERROR);
    }
}
