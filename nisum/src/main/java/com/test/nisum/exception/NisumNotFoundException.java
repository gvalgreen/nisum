package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NisumNotFoundException extends ApiError {

    public NisumNotFoundException() {
        super(ApiResponseCode.NOT_FOUND);
    }
}
