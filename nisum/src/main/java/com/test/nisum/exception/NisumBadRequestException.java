package com.test.nisum.exception;

import com.test.nisum.enums.ApiResponseCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NisumBadRequestException extends ApiError  {


  public NisumBadRequestException() {
    super(ApiResponseCode.BAD_REQUEST);
  }
}
