package com.test.nisum.constants;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public class Constant {

  public static final String API_CURRENT_VERSION = "v1";
  public static final String API = "/api/" + API_CURRENT_VERSION;

  //ENDPOINTS
  public static final String USER = "/user";
}
