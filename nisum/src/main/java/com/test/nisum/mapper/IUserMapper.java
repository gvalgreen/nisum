package com.test.nisum.mapper;

import com.test.nisum.domain.model.User;
import com.test.nisum.dto.UserDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IUserMapper extends IMapperGeneric<User, UserDto> {

  List<UserDto> entityListToDtoList(List<User> userList);
}
