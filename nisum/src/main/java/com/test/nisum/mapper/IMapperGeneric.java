package com.test.nisum.mapper;

import org.mapstruct.MappingTarget;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public interface IMapperGeneric<T, D> {

  T dtoToEntity(D dto);

  D entityToDto(T entity);

  void mergeToEntity(Object entityOld, @MappingTarget T entityNew);

  void mergeToDtos(Object dtoOld, @MappingTarget D dtoNew);
}
