package com.test.nisum.mapper;

import com.test.nisum.domain.model.Phone;
import com.test.nisum.dto.PhoneDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface IPhoneMapper extends IMapperGeneric<Phone, PhoneDto> {

  List<PhoneDto> entityListToDtoList(List<Phone> phoneList);

  List<Phone> dtoListToEntityList(List<PhoneDto> phoneDtoList);
}
