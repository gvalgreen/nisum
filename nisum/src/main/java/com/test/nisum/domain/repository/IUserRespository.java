package com.test.nisum.domain.repository;

import com.test.nisum.domain.model.User;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Repository
public interface IUserRespository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email);

  Optional<User> findById(UUID userId);
}
