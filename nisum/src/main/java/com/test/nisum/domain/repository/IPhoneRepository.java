package com.test.nisum.domain.repository;

import com.test.nisum.domain.model.Phone;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Repository
public interface IPhoneRepository extends JpaRepository<Phone, Long> {

  List<Phone> findByUserId(UUID userId);

}
