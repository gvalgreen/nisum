package com.test.nisum.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Data;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Data
@Entity
public class Phone {

  @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Long id;
  private String number;
  @Column(name = "city_code")
  private String cityCode;
  @Column(name = "country_code")
  private String countryCode;
  @ManyToOne()
  @JoinColumn(name = "user_id")
  private User user;


}
