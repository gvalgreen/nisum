package com.test.nisum.domain.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import lombok.Data;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Data
@Entity
public class User {

  @Id
  private UUID id;
  private String name;
  private String email;
  private String password;
  private UUID token;
  @Column(name = "is_active")
  private boolean isActive;
  private LocalDateTime created;
  private LocalDateTime modified;
  @Column(name = "last_login")
  private LocalDateTime lastLogin;


}
