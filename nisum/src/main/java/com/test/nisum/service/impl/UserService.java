package com.test.nisum.service.impl;

import com.test.nisum.domain.model.User;
import com.test.nisum.domain.repository.IUserRespository;
import com.test.nisum.dto.UserDto;
import com.test.nisum.enums.ApiResponseCode;
import com.test.nisum.exception.Error;
import com.test.nisum.mapper.IUserMapper;
import com.test.nisum.service.IUserService;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Service
@Log4j2
public class UserService implements IUserService {

  IUserRespository userRespository;
  IUserMapper userMapper;

  @Autowired
  public UserService(IUserRespository iUserRespository,
      IUserMapper userMapper) {
    this.userRespository = iUserRespository;
    this.userMapper = userMapper;
  }

  /**
   * Get all users
   *
   * @return Either<Error, List < UserDto>>
   */
  @Override
  public Either<Error, List<UserDto>> findAll() {
    return Try.of(() -> userMapper.entityListToDtoList(userRespository.findAll()))
        .onFailure(throwable -> log.error("Error saving user {}", throwable))
        .toEither(new Error(ApiResponseCode.INTERNAL_ERROR));
  }

  /**
   * Get all users
   *
   * @param email
   * @return Either<Error, List < UserDto>>
   */
  @Override
  public Either<Error, UserDto> findByEmail(String email) {
    Optional<User> userOptional = userRespository.findByEmail(email);
    if (userOptional.isPresent()) {
      return Either.right(userMapper.entityToDto(userOptional.get()));
    }
    return Either.left(new Error(ApiResponseCode.NOT_CONTENT));
  }

  /**
   * Get user by Id
   *
   * @param userId
   * @return Either<Error, UserDto>
   */
  @Override
  public Either<Error, UserDto> findById(UUID userId) {
    Optional<User> userOptional = userRespository.findById(userId);
    if (userOptional.isPresent()) {
      return Either.right(userMapper.entityToDto(userOptional.get()));
    }
    return Either.left(new Error(ApiResponseCode.NOT_CONTENT));
  }


  /**
   * Create User
   *
   * @param userDto
   * @return Either<Error, UserDto>
   */
  @Override
  public Either<Error, UserDto> create(UserDto userDto) {
    return Try
        .of(() -> userMapper.entityToDto(userRespository.save(userMapper.dtoToEntity(userDto))))
        .onFailure(throwable -> log.error("Error saving user {}", throwable))
        .toEither(new Error(ApiResponseCode.INTERNAL_ERROR));
  }

  /**
   * Delete User
   *
   * @param userDto
   */
  @Override
  public void delete(UserDto userDto) {
    Try.run(
        () ->
            userRespository.delete(userMapper.dtoToEntity(userDto))
    )
        .onFailure(throwable -> log.error("Error to delete user {}", throwable));

  }
}
