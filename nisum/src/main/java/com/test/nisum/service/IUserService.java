package com.test.nisum.service;

import com.test.nisum.dto.UserDto;
import com.test.nisum.exception.Error;
import io.vavr.control.Either;
import java.util.List;
import java.util.UUID;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public interface IUserService {

  /**
   * Get all users
   *
   * @return Either<Error, List < UserDto>>
   */
  Either<Error, List<UserDto>> findAll();

  /**
   * Find User by Email
   *
   * @return Either<Error, UserDto>
   */
  Either<Error, UserDto> findByEmail(String email);

  /**
   * Find User by Id
   *
   * @return Either<Error, UserDto>
   */
  Either<Error, UserDto> findById(UUID userId);

  /**
   * Create User
   *
   * @return Either<Error, UserDto>
   */
  Either<Error, UserDto> create(UserDto userDto);


  void delete(UserDto userDto);
}
