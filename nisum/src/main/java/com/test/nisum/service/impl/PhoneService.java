package com.test.nisum.service.impl;

import com.test.nisum.domain.repository.IPhoneRepository;
import com.test.nisum.dto.PhoneDto;
import com.test.nisum.enums.ApiResponseCode;
import com.test.nisum.exception.Error;
import com.test.nisum.mapper.IPhoneMapper;
import com.test.nisum.service.IPhoneService;
import io.vavr.control.Either;
import io.vavr.control.Try;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Service
@Log4j2
public class PhoneService implements IPhoneService {

  IPhoneRepository iPhoneRepository;
  IPhoneMapper iPhoneMapper;

  @Autowired
  public PhoneService(IPhoneRepository iPhoneRepository,
      IPhoneMapper iPhoneMapper) {
    this.iPhoneRepository = iPhoneRepository;
    this.iPhoneMapper = iPhoneMapper;
  }

  /**
   * Get all phones by userId
   *
   * @param userId
   * @return Either<Error, List < PhoneDto>>
   */
  @Override
  public Either<Error, List<PhoneDto>> findAllByUserId(UUID userId) {
    return Try.of(() -> iPhoneMapper.entityListToDtoList(iPhoneRepository.findByUserId(userId)))
        .onFailure(throwable -> log.error("Error getting user's phone {}", throwable))
        .toEither(new Error(ApiResponseCode.INTERNAL_ERROR));
  }

  /**
   * Create Phone
   *
   * @param phoneDtoList
   * @return Either<Error, List < PhoneDto>>
   */
  @Override
  public Either<Error, List<PhoneDto>> create(List<PhoneDto> phoneDtoList) {
    return Try
        .of(() -> iPhoneMapper.entityListToDtoList(
            iPhoneRepository.saveAll(iPhoneMapper.dtoListToEntityList(phoneDtoList))))
        .onFailure(throwable -> log.error("Error saving phones {}", throwable))
        .toEither(new Error(ApiResponseCode.INTERNAL_ERROR));
  }

  /**
   * Delete all Phone
   *
   * @param phoneDtoList
   */
  @Override
  public void delete(List<PhoneDto> phoneDtoList) {
    Try.run(
        () ->
            iPhoneRepository.deleteAll(iPhoneMapper.dtoListToEntityList(phoneDtoList))
    )
        .onFailure(throwable -> log.error("Error deleting phones {}", throwable));

  }
}
