package com.test.nisum.service;

import com.test.nisum.dto.PhoneDto;
import com.test.nisum.exception.Error;
import io.vavr.control.Either;
import java.util.List;
import java.util.UUID;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public interface IPhoneService {

  /**
   * Get all phones by userId
   *
   * @param userId
   * @return Either<Error, List < PhoneDto>>
   */
  Either<Error, List<PhoneDto>> findAllByUserId(UUID userId);

  /**
   * Create Phone
   *
   * @param phoneDtoList
   * @return Either<Error, List < PhoneDto>>
   */
  Either<Error, List<PhoneDto>> create(List<PhoneDto> phoneDtoList);

  /**
   * Delete all Phone
   *
   * @param phoneDtoList
   */
  void delete(List<PhoneDto> phoneDtoList);
}
