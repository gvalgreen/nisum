package com.test.nisum.delegate.impl;

import com.test.nisum.dto.ApiResponseDTO;
import com.test.nisum.dto.Notification;
import com.test.nisum.enums.ApiResponseCode;
import com.test.nisum.exception.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class BaseDelegate {

  ResponseEntity<ApiResponseDTO> error(ApiResponseCode apiResponseCode) {
    Notification notification = Notification
        .builder(apiResponseCode.getDescription()).build();
    return ResponseEntity.status(apiResponseCode.getHttpStatus())
        .body(new ApiResponseDTO<>(notification));
  }

  ResponseEntity<ApiResponseDTO> error(Error error) {
    Notification notification = Notification
        .builder(error.getMessage()).build();

    switch (error.getHttpStatus().value()) {
      case 204:
        return ResponseEntity.status(HttpStatus.NO_CONTENT)
            .body(new ApiResponseDTO<>(notification));
      case 404:
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
            .body(new ApiResponseDTO<>(notification));
      case 400:
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
            .body(new ApiResponseDTO<>(notification));

      default:
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ApiResponseDTO<>(error.getMessage(), notification));
    }


  }

  ResponseEntity<ApiResponseDTO> success(Object data) {

    return ResponseEntity.ok(new ApiResponseDTO<>(data, Notification
        .builder(ApiResponseCode.SUCCESS.getDescription())
        .build()));
  }
}
