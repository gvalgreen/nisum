package com.test.nisum.delegate.impl;

import com.test.nisum.delegate.IUserDelegate;
import com.test.nisum.dto.ApiResponseDTO;
import com.test.nisum.dto.PhoneDto;
import com.test.nisum.dto.UserDto;
import com.test.nisum.enums.ApiResponseCode;
import com.test.nisum.exception.Error;
import com.test.nisum.service.IPhoneService;
import com.test.nisum.service.IUserService;
import io.vavr.control.Either;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Component
@Log4j2
public class UserDelegate extends BaseDelegate implements IUserDelegate {

  private IUserService userService;
  private IPhoneService phoneService;

  @Autowired
  public UserDelegate(IUserService userService,
      IPhoneService phoneService) {
    this.userService = userService;
    this.phoneService = phoneService;
  }

  /**
   * Get all users
   *
   * @return ApiResponseDTO
   */
  @Override
  public ResponseEntity<ApiResponseDTO> findAll() {
    final Either<Error, List<UserDto>> usuarioDTOEither = userService.findAll();
    if (usuarioDTOEither.isRight()) {
      if (usuarioDTOEither.get().isEmpty()) {
        log.info("findAll:: {}", ApiResponseCode.NOT_CONTENT.getHttpStatus());
        return error(ApiResponseCode.NOT_CONTENT);
      }
      log.info("findAll:: {}", ApiResponseCode.SUCCESS.getHttpStatus());
      return success(usuarioDTOEither.get());
    }
    return error(usuarioDTOEither.getLeft());
  }

  /**
   * Create an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  @Override
  public ResponseEntity<ApiResponseDTO> create(UserDto userDto) {

    final Either<Error, UserDto> emailEither = userService.findByEmail(userDto.getEmail());
    if (emailEither.isRight()) {
      return error(ApiResponseCode.EMAIL_REGISTERED);
    }

    userDto.setId(UUID.randomUUID());
    userDto.setToken(UUID.randomUUID());
    userDto.setCreated(LocalDateTime.now());
    userDto.setModified(LocalDateTime.now());
    userDto.setLastLogin(LocalDateTime.now());
    userDto.setActive(true);
    final Either<Error, UserDto> usuarioDTOEither = userService.create(userDto);
    if (usuarioDTOEither.isRight()) {
      log.info("create:: user created successfully {}", ApiResponseCode.SUCCESS.getHttpStatus());
      userDto.getPhones().forEach(phone -> phone.setUser(usuarioDTOEither.get()));
      final Either<Error, List<PhoneDto>> phoneDtoEitherList = phoneService
          .create(userDto.getPhones());
      if (phoneDtoEitherList.isRight()) {
        log.info("create:: phones created successfully {}",
            ApiResponseCode.SUCCESS.getHttpStatus());
        usuarioDTOEither.get().setPhones(phoneDtoEitherList.get());
        return success(usuarioDTOEither.get());
      }
      return error(phoneDtoEitherList.getLeft());
    }
    return error(usuarioDTOEither.getLeft());
  }

  /**
   * Update an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  @Override
  public ResponseEntity<ApiResponseDTO> update(UserDto userDto) {
    final Either<Error, UserDto> usuarioDTOEither = userService.findByEmail(userDto.getEmail());
    if (usuarioDTOEither.isRight()) {
      usuarioDTOEither.get().setModified(LocalDateTime.now());
      usuarioDTOEither.get().setName(userDto.getName());
      usuarioDTOEither.get().setPassword(userDto.getPassword());
      usuarioDTOEither.get().setActive(userDto.isActive());
      final Either<Error, UserDto> usuarioUpdateDTOEither = userService
          .create(usuarioDTOEither.get());

      if (usuarioUpdateDTOEither.isRight()) {
        log.info("update:: user update successfully {}", ApiResponseCode.SUCCESS.getHttpStatus());
        return success(usuarioUpdateDTOEither.get());
      }
      return error(usuarioUpdateDTOEither.getLeft());
    }

    return error(ApiResponseCode.EMAIL_NOT_FOUND);
  }

  /**
   * Delete an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  @Override
  public ResponseEntity<ApiResponseDTO> delete(UserDto userDto) {
    final Either<Error, UserDto> usuarioDTOEither = userService.findById(userDto.getId());
    if (usuarioDTOEither.isRight()) {

      final Either<Error, List<PhoneDto>> phoneDtoEitherList = phoneService
          .findAllByUserId(usuarioDTOEither.get().getId());
      if (phoneDtoEitherList.isRight()) {
        phoneService.delete(phoneDtoEitherList.get());
        log.info("delete:: user's phones {} deleted", userDto.getId());
        userService.delete(usuarioDTOEither.get());
        log.info("delete:: user {} deleted", userDto.getId());
        return error(ApiResponseCode.SUCCESS);
      }

      return error(ApiResponseCode.USER_DELETE_ERROR);
    }
    return error(usuarioDTOEither.getLeft());
  }



}
