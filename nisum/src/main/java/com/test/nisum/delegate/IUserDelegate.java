package com.test.nisum.delegate;

import com.test.nisum.dto.ApiResponseDTO;
import com.test.nisum.dto.UserDto;
import org.springframework.http.ResponseEntity;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
public interface IUserDelegate {

  /**
   * Get all users
   *
   * @return ApiResponseDTO
   */
  ResponseEntity<ApiResponseDTO> findAll();

  /**
   * Create an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  ResponseEntity<ApiResponseDTO> create(UserDto userDto);

  /**
   * Update an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  ResponseEntity<ApiResponseDTO> update(UserDto userDto);

  /**
   * Delete an User
   *
   * @param userDto
   * @return ApiResponseDTO
   */
  ResponseEntity<ApiResponseDTO> delete(UserDto userDto);

}
