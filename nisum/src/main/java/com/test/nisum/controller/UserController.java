package com.test.nisum.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.test.nisum.constants.Constant;
import com.test.nisum.delegate.IUserDelegate;
import com.test.nisum.dto.ApiResponseDTO;
import com.test.nisum.dto.UserDto;
import com.test.nisum.dto.Views;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gvalgreen
 * @version 1.0.0
 */
@Api(value = "Gestión de usuarios")
@RestController
@RequestMapping(Constant.API + Constant.USER)
@CrossOrigin()
public class UserController {

  @Autowired
  IUserDelegate userDelegate;


  /**
   * Get all users
   */
  @ApiOperation(value = "Users List", response = UserDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 204, message = "There isn't the resource")
  })
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ApiResponseDTO> getAllUsers() {
    return userDelegate.findAll();

  }

  /**
   * Create an User
   */
  @ApiOperation(value = "Create User", response = UserDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 204, message = "There isn't the resource")
  })
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @JsonView(Views.UserViews.class)
  public ResponseEntity<ApiResponseDTO> create(@Valid @RequestBody UserDto userDto) {
    return userDelegate.create(userDto);

  }

  /**
   * Update an User
   */
  @ApiOperation(value = "Update User", response = UserDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 204, message = "There isn't the resource")
  })
  @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @JsonView(Views.UserViews.class)
  public ResponseEntity<ApiResponseDTO> update(@Valid @RequestBody UserDto userDto) {
    return userDelegate.update(userDto);

  }

  /**
   * Delete an User
   */
  @ApiOperation(value = "Delete User", response = UserDto.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Success"),
      @ApiResponse(code = 204, message = "There isn't the resource")
  })
  @DeleteMapping()
  public ResponseEntity<ApiResponseDTO> delete(@RequestBody UserDto userDto) {
    return userDelegate.delete(userDto);
  }


}
