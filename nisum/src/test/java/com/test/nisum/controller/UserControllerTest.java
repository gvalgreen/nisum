package com.test.nisum.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.nisum.constants.Constant;
import com.test.nisum.delegate.IUserDelegate;
import com.test.nisum.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc
class UserControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  IUserDelegate userDelegate;

  @Autowired
  ObjectMapper objectMapper;

  UserDto userDto;

  @BeforeEach
  public void setUp() {
    userDto = new UserDto();
    userDto.setName("Juan Rodriguez");
    userDto.setEmail("juan@rodriguezorddg.co");
    userDto.setPassword("Hunter12");
  }


  @Test
  public void testGetAllUsers() throws Exception {
    mvc.perform(MockMvcRequestBuilders.get(Constant.API + Constant.USER)
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }

  @Test
  public void testCreate() throws Exception {
    when(userDelegate.create(any())).thenReturn(new ResponseEntity(userDto, HttpStatus.OK));
    mvc.perform(MockMvcRequestBuilders.post(Constant.API + Constant.USER)
        .content(objectMapper.writeValueAsString(userDto))
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }

  @Test
  public void testUpdate() throws Exception {
    when(userDelegate.create(any())).thenReturn(new ResponseEntity(userDto, HttpStatus.OK));
    mvc.perform(MockMvcRequestBuilders.put(Constant.API + Constant.USER)
        .content(objectMapper.writeValueAsString(userDto))
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }

  @Test
  public void testDelete() throws Exception {
    mvc.perform(MockMvcRequestBuilders.delete(Constant.API + Constant.USER)
        .content(objectMapper.writeValueAsString(userDto))
        .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
  }

}