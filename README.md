# Nisum Test

API RESTful creación de usuarios:
  - Crear usuarios.
  - Visualizar usuarios creados..
  - Editar usuario.
  - Eliminar usuario.

# Entregables incluidos en este repositorio

- Códifo fuente.
- Nisum.postman_collection.json
- script.sql
- Diagrama de la solución
- Readme.md

## Documentación del API

- Ingresando a /swagger-ui.html

## Criterios
 
  1. Responder el código de status HTTP adecuado
  2. En caso de éxito, retorne el usuario y los siguientes campos:
		○ id: id del usuario (puede ser lo que se genera por el banco de datos, pero sería
		más deseable un UUID)
		○ created: fecha de creación del usuario
		○ modified: fecha de la última actualización de usuario
		○ last_login: del último ingreso (en caso de nuevo usuario, va a coincidir con la
		fecha de creación)
		○ token: token de acceso de la API (puede ser UUID o JWT)
		○ isactive: Indica si el usuario sigue habilitado dentro del sistema.
  3. Si caso el correo conste en la base de datos, deberá retornar un error "El correo ya registrado".
  4.El correo debe seguir una expresión regular para validar que formato sea el correcto. (aaaaaaa@dominio.cl)

 ## Estructura general del response:
  
  JSON con 2 key: 
  - data: Contiene los datos del usuario , resultante luego de consumir el endpoint
  - notification: Contienen el mensaje  resultante luego de consumir el endpoint
  
  ```json
  {
      "data": {
          
      },
      "notification": {
          "message": "Request executed successfully"
      }
  }
  ```
## Estructura general del response de validación de campos:

JSON con 3 key: 
  - timestamp: Marca de tiempo
  - status: Estado HTTP
  - message: Detalle del mensaje de validación de campos

 ```json
{
    "timestamp": "2020-08-28T16:14:29.415+00:00",
    "status": 400,
    "message": [
        "Password format is invalid. the correct format is: 1 UPPERCASE, any lowercase and 2 numbers",
        "Email format is invalid",
        "Please provide a name"
    ]
}
  ```

## Uso

En la colección de postman se encuentran 4 endpoints:

### Crear Usuario 

- Endpoint: api/v1/user
- Método: POST

Request:

```json
{
    "name": "Juan Rodriguez",
    "email": "juan@rodriguezor.co",
    "password": "Hunter12",
    "phones": [
        {
            "number": "1234567",
            "cityCode": "1",
            "countryCode": "57"
        },
        {
            "number": "12345678",
            "cityCode": "1",
            "countryCode": "57"
        }
    ]
}
  ```
Response:

```json
{
    "data": {
        "id": "d93498de-9173-49e5-bdc5-d785dd5c5da6",
        "isActive": true,
        "token": "ed3ce28d-ae63-4a6d-9e8e-8094bfadc65f",
        "created": "2020-08-28T10:54:11.674",
        "modified": "2020-08-28T10:54:11.674",
        "lastLogin": "2020-08-28T10:54:11.674"
    },
    "notification": {
        "message": "Request executed successfully"
    }
}
 ```

 ### Editar Usuario 

- Endpoint: api/v1/user
- Método: PUT

Request:

```json
{
    "name": "Juan Rodriguez",
    "email": "juan@rodriguezor.co",
    "password": "Hunter12",
    "phones": [
        {
            "number": "1234567",
            "cityCode": "1",
            "countryCode": "57"
        },
        {
            "number": "12345678",
            "cityCode": "1",
            "countryCode": "57"
        }
    ]
}
  ```
Response:

```json
{
    "data": {
        "id": "d93498de-9173-49e5-bdc5-d785dd5c5da6",
        "isActive": true,
        "token": "ed3ce28d-ae63-4a6d-9e8e-8094bfadc65f",
        "created": "2020-08-28T10:54:11.674",
        "modified": "2020-08-28T10:54:11.674",
        "lastLogin": "2020-08-28T10:54:11.674"
    },
    "notification": {
        "message": "Request executed successfully"
    }
}
 ```

### Consultar Usuarios creados 

- Endpoint: api/v1/user
- Método: GET


Response:

```json
{
    "data": [
        {
            "id": "f42ca951-dc6a-4fb9-a642-9b6171a7809b",
            "isActive": true,
            "token": "ec72c393-4e11-4333-8a86-0470bf7f3013",
            "created": "2020-08-28T10:53:58.36",
            "modified": "2020-08-28T10:53:58.36",
            "lastLogin": "2020-08-28T10:53:58.36"
        },
        {
            "id": "d93498de-9173-49e5-bdc5-d785dd5c5da6",
            "isActive": true,
            "token": "ed3ce28d-ae63-4a6d-9e8e-8094bfadc65f",
            "created": "2020-08-28T10:54:11.674",
            "modified": "2020-08-28T10:54:11.674",
            "lastLogin": "2020-08-28T10:54:11.674"
        }
    ],
    "notification": {
        "message": "Request executed successfully"
    }
}
 ```

### Eliminar Usuario 

- Endpoint: api/v1/user
- Método: DELETE

Request:

- id: id de ususario

```json
{
    "id": "40ccb8e0-9ba9-42a4-8d21-387211f2a2ab"
}
  ```
Response:

```json
{
    "data": null,
    "notification": {
        "message": "Request executed successfully"
    }
}
 ```
 


## Unit Test

Se realizó cobertura a la clase UserController
  

## TODO

  1. Por realizar: construir servicio de login con Spring security.


## Credits

@gvalgreen  - Gustavo Valverde 2020

## License

Copyright (c) 2020 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.